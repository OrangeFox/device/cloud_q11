# Exports
export aryan=$PWD && echo " " && echo "Current Directory: $aryan"
export backup_location="Partition Backup Intex- $(date +'%b %d %Y %H-%M')"
export start_time=$(date +'%b %d %Y %H.%M.%S')

# by-name folder location
export block_location=/dev/block/platform/mtk-msdc.0/11120000.msdc0/by-name

echo " " && echo " " && echo "Creating Backup directory..."
mkdir -p "$backup_location"

echo " " && echo " " && echo -e "\n\nBacking Up Partitions Started..."

# Main backup
for part in proinfo nvram protect1 protect2 seccfg lk para logo expdb frp nvdata metadata oemkeystore secro keystore boot recovery flashinfo; do
echo " " && echo " " && echo -e "\n\nBacking up Partition: $part" && dd if="$block_location/$part" of="$backup_location/$part"
done

# Fix Permissions
cd "$backup_location" && chmod 664 * && cd ..

echo " " && echo " " && echo -e "\n\nTime To Archive :)\n\n"

# Archive time using tar.gz
tar --use-compress-program="pigz -k1" -cf "$backup_location.tar.gz" "$backup_location" || tar -cf "$backup_location.tar" "$backup_location" || echo "Oops pigz and tar isn't installed install them first...."

# Cleanup
if [ -f "`echo $backup_location.tar.gz`" ]; then rm -rf "$backup_location" && mv "$backup_location.tar.gz" /sdcard; else echo "Seems like gzip compression isn't used"; fi
if [ -f "`echo $backup_location.tar`" ]; then rm -rf "$backup_location" && mv "$backup_location.tar" /sdcard; else echo Seems like gzip compression is used; fi

export end_time=$(date +'%b %d %Y %H.%M.%S')

echo " " && echo -e "\n\nHurray Partition Backup Done...."

echo " " && echo " "
echo -e "\n\nStart Time : $start_time\n"
echo -e "End Time : $end_time\n"
echo " "
