for var in eng user userdebug; do
  add_lunch_combo omni_Cloud_Q11-$var
done

export ALLOW_MISSING_DEPENDENCIES=true
export FOX_USE_TWRP_RECOVERY_IMAGE_BUILDER=1
export LC_ALL="C"
export TARGET_ARCH=arm
export OF_TARGET_DEVICES="Cloud_Q11,waterfall"
export TARGET_DEVICE_ALT="waterfall"
export OF_USE_MAGISKBOOT_FOR_ALL_PATCHES=1
export OF_QUICK_BACKUP_LIST="/data;/boot;/system;"


# Miui Specific featurres disabled

#export OF_TWRP_COMPATIBILITY_MODE=1 #As it is include in (OF_DISABLE_MIUI_SPECIFIC_FEATURES=1)
export OF_DISABLE_MIUI_SPECIFIC_FEATURES=1
export OF_NO_MIUI_OTA_VENDOR_BACKUP=1
export OF_DISABLE_MIUI_OTA_BY_DEFAULT=1


export OF_MAINTAINER="Aryan Karan"
export FOX_VERSION="R11.1"
export FOX_USE_NANO_EDITOR=1
export FOX_USE_BASH_SHELL=1
export FOX_USE_TAR_BINARY=1
export OF_DONT_KEEP_LOG_HISTORY=1
export OF_USE_LOCKSCREEN_BUTTON=1
export FOX_USE_GREP_BINARY=1
export FOX_USE_XZ_UTILS=1
export FOX_REPLACE_TOOLBOX_GETPROP=1
#export OF_PATCH_AVB20=1
#torch OF_FL_PATH1 & OF_FL_PATH2
#export OF_FL_PATH1="/sys/class/flashlightdrv/dev"

# Stable version release
export FOX_BUILD_TYPE="Stable"

# Extras
export OF_RUN_POST_FORMAT_PROCESS=1

export FOX_DEVICE_MODEL="Intex Cloud Q11"

#export OF_SCREEN_H=1280  # Defined Screen resolution ri8 now ommited

# Partition Info
export FOX_RECOVERY_INSTALL_PARTITION="/dev/block/platform/mtk-msdc.0/11120000.msdc0/by-name/recovery"
export FOX_RECOVERY_SYSTEM_PARTITION="/dev/block/platform/mtk-msdc.0/11120000.msdc0/by-name/system"
export FOX_RECOVERY_VENDOR_PARTITION="/dev/block/platform/mtk-msdc.0/11120000.msdc0/by-name/vendor"
export FOX_RECOVERY_BOOT_PARTITION="/dev/block/platform/mtk-msdc.0/11120000.msdc0/by-name/boot"

#export FOX_DISABLE_APP_MANAGER=1
export OF_FLASHLIGHT_ENABLE=0

# No green LED Settings
export OF_USE_GREEN_LED=0

# Magisk v23.0
export FOX_USE_SPECIFIC_MAGISK_ZIP="$PWD/device/intex/Cloud_Q11/Magisk/Magisk.zip"
#export FOX_REMOVE_AAPT=1

# Use persist only when needed

export OF_DEVICE_WITHOUT_PERSIST=1
export FSTAB="$PWD/device/intex/Cloud_Q11/recovery.fstab"

# Persist Lines
export PERSIST_LINE='/persist            ext4      /dev/block/platform/mtk-msdc.0/11120000.msdc0/by-name/persist         flags=display="Persist";backup=1;wipeingui;removable;storage;'
export PERSIST_IMAGE_LINE='/persist_image      emmc      /dev/block/platform/mtk-msdc.0/11120000.msdc0/by-name/persist         flags=display="Persist Image";backup=1;flashimg=1;'

if [ $OF_DEVICE_WITHOUT_PERSIST = 0 ]; then
   if [ "$(grep "$PERSIST_LINE" "$FSTAB")" = "$PERSIST_LINE" ]; then export persist_line_exists=1; else export persist_line_exists=0; fi
   if [ "$(grep "$PERSIST_IMAGE_LINE" "$FSTAB")" = "$PERSIST_IMAGE_LINE" ]; then export persist_image_line_exists=1; else export persist_image_line_exists=0; fi
   if [ $persist_line_exists = 0 ]; then
      echo -e " " >> "$FSTAB"
      echo "# Suedo Persist" >> "$FSTAB"
      echo "$PERSIST_LINE" >> "$FSTAB"
   fi
   if [ $persist_image_line_exists = 0 ]; then
      echo "$PERSIST_IMAGE_LINE" >> "$FSTAB";
   fi
   unset persist_line_exists persist_image_line_exists;
fi

export BUILD_USERNAME=aryan && export BUILD_HOSTNAME=aryankaran-HP-245-G7-notebook
